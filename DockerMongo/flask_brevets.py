"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

import flask
from flask import request
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
import os
from pymongo import MongoClient # mongodb connection
import logging

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

client = MongoClient("mongodb://localhost:27017")
# client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017) # <<<<<<<<<<<<<<<<< REMEMBER TO SWITCH
db = client.times


########
# Pages
########

@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############

def make_arrow(date: str, time: str) -> "Arrow":
    year = int(date[:4])
    month = int(date[5:7])
    day = int(date[8:])
    hour = int(time[:2])
    minute = int(time[3:])
    return arrow.Arrow(year, month, day, hour, minute)

@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    app.logger.debug("request.args: {}".format(request.args))
    # get input brevet distance for race
    distance = request.args.get("brevet", type=str)
    # remove "km" off string ("200km" -> 200) and turn into int
    distance = int(distance[:-2])
    app.logger.debug("distance={}".format(distance))
    # get the input km for the race
    km = request.args.get('km', 999, type=float)
    app.logger.debug("km={}".format(km))
    race_date = request.args.get("date", type=str)
    app.logger.debug("race start date={}".format(race_date))
    race_time = request.args.get("time", type=str)
    app.logger.debug("race start time={}".format(race_time))
    start_race = make_arrow(race_date, race_time)
    app.logger.debug("iso-format race start={}".format(start_race.isoformat()))
    open_time = acp_times.open_time(km, distance, start_race)
    close_time = acp_times.close_time(km, distance, start_race)
    result = {"open": open_time, "close": close_time}
    app.logger.debug(f"times: {result}")
    return flask.jsonify(result=result)


######################
# MongoDB Interaction
######################

@app.route("/_store")
def _save_times():
    raw_times = request.args.to_dict()
    app.logger.debug(f"times={raw_times}")
    # calculate num times to insert
    num_times = len(raw_times)/2
    # don't insert empty time data
    if num_times > 0:
        times = {"open": [], "close": []}
        for time in raw_times:
            if raw_times[time] == "open":
                times["open"].append(time)
            else:
                times["close"].append(time)
        # remove old times
        db.times.remove({})
        # add new time
        db.times.insert_one(times)
    # send number of open/close times inserted
    return flask.jsonify(result={"ok": num_times})


@app.route("/times")
def display_times():
    _items = db.times.find()
    item = next(_items)
    num_times = len(item["open"])
    return flask.render_template("times.html", times=item, num_times=num_times)



#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
