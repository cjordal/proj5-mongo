# Project 4: Brevet time calculator with Ajax

Implementation of the RUSA ACP controle time calculator with flask and ajax.

Calculates control times at given distances from a given starting time. Also has a submit button and 
display button that allows user to submit times to a database and display them on a separate page.

Modified by Cameron Jordal (cjordal@uoregon.edu).
Credits to Michal Young for the initial version of this code.

## Logic 

### General Rules

The logic used to describe the oiginal implimentation of the brevet control calculator (https://rusa.org/pages/acp-brevet-control-times-calculator) was quite vague. The logic use to calculate brevet control times, however, is quite simple and is described here.

There are opening times and closing times for races. Both of which can be calculated besed on speed (km/hr) at different intervals. For opening times, The Intervals are:
	
	0-200km:    34 km/hr
	201-400km:  32 km/hr
	401-600km:  30 km/hr
	601-1000km: 28 km/hr

The closing times for races are slighly more complicated. The closing time at the start of the race is 1 hr, then follows the intervals:

	0-60km:     20 km/hr
	61-200km:   15 km/hr
	201-400km:  15 km/hr
	401-600km:  15 km/hr
	601-1000km: 11.428 km/hr

To calculate the opening and close times you simply multiply the speed * kilometers travelled. For example, if a control is at 500 km, the opening time would be:

	(200km / 34km/hr) + (200km / 32km/hr) + (100km / 30km/hr) = 15hr 28min

And the closing time would be:

	1 + (60km / 20km/hr) + (440km / 15km/hr) = 33hr 20min

### Notes

When the ending control distance exceeds the brevet distance (control = 611km, but a 600km brevet), the brevet distance is used to calculate the final control distance, not the control distance. For example, that 600km brevet with a 611km control would have an opening/closing time of 

    (200km / 34km/hr) + (200km / 32km/hr) + (200km / 30km/hr) = 18hr 48min
    1 + (60 / 20km/hr) + (540 / 15 km/hr) = 40hr

For a 200km brevet, the ending time is always 13hr 30min (even though 200km / 15km/hr = 13hr 20min)

## Website

### Description

The website allows for the user to input all the necessary values for their calculation and computes
the results immediately. It also is where u can submit times and then click a button to go see a 
page with just those times.

### Notes

In the event that the user tries to submit times when nothing was given, the website will not allow the user 
to see the displayed times page. It will simply stay on that same page.

## Database

### Description

Simple MongoDB database. Current implementation saves user times and then returns them upon request.

### Notes

Current interaction does not support multiple users, as user data is not stored in the database in a user-specific way. 
Old user data is also deleted prior to each submission.
